<?php

namespace Avris\Suml;

use Avris\Suml\Exception\ParseException;
use Avris\Suml\Exception\RuntimeException;

/**
 * @covers \Avris\Suml\Suml
 */
final class Suml implements ParserInterface, DumperInterface
{
    /** @var ParserInterface */
    private $parser;

    /** @var DumperInterface */
    private $dumper;

    public function __construct(?ParserInterface $parser = null, ?DumperInterface $dumper = null)
    {
        $this->parser = $parser ?: new Parser();
        $this->dumper = $dumper ?: new Dumper();
    }

    public function parse(string $value)
    {
        return $this->parser->parse($value);
    }

    public function parseFile(string $filename)
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            throw new RuntimeException(sprintf('File "%s" is not readable', $filename));
        }

        try {
            return $this->parser->parse(file_get_contents($filename), $filename);
        } catch (ParseException $e) {
            throw $e->withFilename($filename);
        }
    }

    public function dump($value): string
    {
        return $this->dumper->dump($value);
    }
}
