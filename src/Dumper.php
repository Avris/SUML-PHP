<?php

namespace Avris\Suml;

use Avris\Suml\Exception\DumpException;

/**
 * @covers \Avris\Suml\Dumper
 */
final class Dumper implements DumperInterface
{
    const INDENT = '    ';
    const MAX_COLUMNS = 120;
    
    public function dump($value): string
    {
        return $this->doDump($value, 0)->writeln()->get();
    }

    private function doDump($value, int $indent): StringBuffer
    {
        $buffer = new StringBuffer();

        if ($value instanceof \JsonSerializable) {
            $value = $value->jsonSerialize();
        }

        if (!$this->isIterable($value)) {
            $buffer->write($indent ? ' ' : '')->write($this->dumpScalar($value, $indent));

            return $buffer;
        }

        $value = $this->toArray($value);

        if (count($value) === 0) {
            $buffer->write($indent ? ' ' : '')->write('[]');

            return $buffer;
        }

        if ($indent) {
            $buffer->writeln();
        }

        if ($this->isDict($value)) {
            foreach ($value as $k => $v) {
                $buffer->write('%s%s:%s', str_repeat(self::INDENT, $indent), $k, $this->doDump($v, $indent + 1));
                if (!$this->isLastKey($value, $k)) {
                    $buffer->writeln();
                }
            }

            return $buffer;
        }

        foreach ($value as $i => $v) {
            $buffer->write(
                '%s-%s',
                str_repeat(self::INDENT, $indent),
                $this->doDump($v, $indent + 1)
            );
            if (!$this->isLastKey($value, $i)) {
                $buffer->writeln();
            }
        }

        return $buffer;
    }

    private function isLastKey(array $array, $key)
    {
        $keys = array_keys($array);

        return $key === end($keys);
    }

    private function isIterable($value): bool
    {
        return is_array($value) || $value instanceof \Traversable;
    }

    private function toArray($value): array
    {
        return $value instanceof \Traversable ? iterator_to_array($value) : $value;
    }

    private function isDict($value): bool
    {
        return array_keys($value) !== range(0, count($value) - 1);
    }

    private function dumpScalar($scalar, int $indent): string
    {
        if ($scalar === null) {
            return '~';
        }

        if ($scalar === true) {
            return 'true';
        }

        if ($scalar === false) {
            return 'false';
        }

        if ($scalar === INF) {
            return 'inf';
        }

        if ($scalar === -INF) {
            return '-inf';
        }

        if (gettype($scalar) === 'integer' || gettype($scalar) === 'double') {
            return (string) $scalar;
        }

        if ($scalar instanceof \DateTimeInterface) {
            return $this->dumpDateTime($scalar);
        }

        if (gettype($scalar) === 'string') {
            return $this->quoteString($scalar, $indent);
        }

        throw new DumpException($scalar);
    }

    private function dumpDateTime(\DateTimeInterface $dateTime): string
    {
        $result = [];
        $date = $dateTime->format('Y-m-d');
        if ($date !== '0001-01-01') {
            $result[] = $date;
        }

        $time = $dateTime->format('H:i:s');
        if ($time !== '00:00:00') {
            $result[] = $time . $dateTime->format('O');
        }

        return join(' ', $result);
    }

    private function quoteString(string $string, int $indent): string
    {
        $string = rtrim(str_replace(["\r\n", "\r"], "\n", $string), "\n");

        if (strpos($string, "\n") !== false) {
            $buffer = new StringBuffer();
            $buffer->writeln('|');
            $lines = explode("\n", $string);
            foreach ($lines as $i => $line) {
                if ($line) {
                    $buffer->write('%s%s', str_repeat(self::INDENT, max($indent, 1)), $line);
                }
                if (!$this->isLastKey($lines, $i)) {
                    $buffer->writeln();
                }
            }

            return $buffer->get();
        }

        $maxWidth = self::MAX_COLUMNS - $indent * mb_strlen(self::INDENT);
        if (mb_strlen($string) > $maxWidth) {
            $buffer = new StringBuffer();
            $buffer->writeln('>')->write(str_repeat(self::INDENT, $indent));
            $line = new StringBuffer();
            $keepTrailingSpaces = false;
            foreach (explode(" ", $string) as $word) {
                if ($word === '') {
                    $buffer->write(' ');
                    $line->write(' ');
                    $keepTrailingSpaces = true;
                    continue;
                }

                if ($keepTrailingSpaces) {
                    $buffer->write($word);
                    $keepTrailingSpaces = false;
                    continue;
                }

                if ($line->length() + mb_strlen($word) > $maxWidth) {
                    $buffer->writeln()->write(str_repeat(self::INDENT, max($indent, 1)));
                    $line->clear();
                }

                $buffer->write($line->length() ? ' ' : '')->write($word);
                $line->write($line->length() ? ' ' : '')->write($word);
            }

            return $buffer->get();
        }

        return "'" . str_replace("'", "''", $string) . "'";
    }
}
