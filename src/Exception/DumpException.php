<?php

namespace Avris\Suml\Exception;

final class DumpException extends \Exception implements SumlException
{
    /** @var mixed */
    private $value;

    public function __construct($value)
    {
        $this->value = $value;

        $messageParts = ['Cannot dump value:', gettype($value)];
        if (gettype($value) === 'object') {
            $messageParts[] = get_class($value);
            if (method_exists($value, '__toString')) {
                $messageParts[] = '"' . mb_substr($value, 0, 16) .  '..."';
            }
        }

        parent::__construct(join(' ', array_filter($messageParts)));
    }

    public function getValue()
    {
        return $this->value;
    }
}
