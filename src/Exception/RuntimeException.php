<?php

namespace Avris\Suml\Exception;

final class RuntimeException extends \RuntimeException implements SumlException
{
}
