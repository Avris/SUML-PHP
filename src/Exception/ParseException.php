<?php

namespace Avris\Suml\Exception;

final class ParseException extends \Exception implements SumlException
{
    /** @var string */
    private $orgMessage;

    /** @var int */
    private $orgLine;

    /** @var string */
    private $orgContext;

    /** @var string|null */
    private $orgFilename;

    public function __construct(
        string $message = '',
        int $line = -1,
        string $context = '',
        ?string $filename = null
    )
    {
        $this->orgMessage = $message;
        $this->orgLine = $line;
        $this->orgContext = $context;
        $this->orgFilename = $filename;

        $messageParts = ['Cannot parse'];
        if ($filename) {
            $messageParts[] = sprintf('file "%s"', $filename);
        }
        if ($line >= 0) {
            $messageParts[] = sprintf('line %s', $line + 1);
        }
        if ($context) {
            $messageParts[] = sprintf('near `%s`', $context);
        }
        if ($message) {
            $messageParts[] = ': ' . $message;
        }

        parent::__construct(join(' ', $messageParts));
    }

    public function withFilename(string $filename): self
    {
        return new self($this->orgMessage, $this->orgLine, $this->orgContext, $filename);
    }

    public function withLine(int $line): self
    {
        return new self($this->orgMessage, $line, $this->orgContext, $this->orgFilename);
    }
}
