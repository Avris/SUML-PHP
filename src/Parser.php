<?php

namespace Avris\Suml;

use Avris\Suml\Exception\ParseException;

/**
 * @covers \Avris\Suml\Dumper
 */
final class Parser implements ParserInterface
{
    const INDENT = 4;

    const CONTEXT_NONE = 0;
    const CONTEXT_TEXT_VERBATIM = 1;
    const CONTEXT_TEXT_FOLDED = 2;

    const CONTEXTS = [
        '' => self::CONTEXT_NONE,
        '|' => self::CONTEXT_TEXT_VERBATIM,
        '>' => self::CONTEXT_TEXT_FOLDED,
    ];

    const REGEX_EMPTY = '/^ *(#.*)?$/u';

    const REGEX_PART_COMMENT = '(?: *#.*?)?$/u';
    const REGEX_PART_DATE = '(?:\d\d\d\d-\d\d-\d\d)';
    const REGEX_PART_TIME = '(?:\d\d:\d\d:\d\d(?:[+-]\d\d\d\d)?)';
    const REGEX_PART_DICT_KEY = '([^:#\' {}]+):';
    const REGEX_PART_INLINE_ELEMENT = '((?:[^,\']+)|(?:\'[^\']*\'))\s*,?';

    const REGEX_NULL = '~';
    const REGEX_INT_DEC = '/^([+-]?[0-9]+)' . self::REGEX_PART_COMMENT;
    const REGEX_INT_BIN = '/^([+-])?0b([0-1]+)' . self::REGEX_PART_COMMENT;
    const REGEX_INT_OCT = '/^([+-])?0o([0-7]+)' . self::REGEX_PART_COMMENT;
    const REGEX_INT_HEX = '/^([+-])?0x([0-9A-Ea-e]+)' . self::REGEX_PART_COMMENT;
    const REGEX_INF = '/^([+-])?inf' . self::REGEX_PART_COMMENT;
    const REGEX_FLOAT = '/^([+-]?[0-9]*\.[0-9]*([Ee][+-][0-9]+)?)' . self::REGEX_PART_COMMENT;
    const REGEX_BOOL = '/^(true|false)?' . self::REGEX_PART_COMMENT;
    const REGEX_DATETIME = '/^(' . self::REGEX_PART_DATE . '|' . self::REGEX_PART_TIME
        . '|' . self::REGEX_PART_DATE . ' ' . self::REGEX_PART_TIME . '|(?:@\d+))' . self::REGEX_PART_COMMENT;
    const REGEX_STRING_INLINE = '/^\'((?:[^\']|\'\')*)\'' . self::REGEX_PART_COMMENT;
    const REGEX_STRING_BLOCK = '/^([>|])' . self::REGEX_PART_COMMENT;
    const REGEX_DICT = '/^' . self::REGEX_PART_DICT_KEY . '( .*?|)$/u';
    const REGEX_LIST_INLINE = '/^\[(.*)\]' . self::REGEX_PART_COMMENT;
    const REGEX_DICT_INLINE = '/^\{(.*)\}' . self::REGEX_PART_COMMENT;
    const REGEX_LIST = '/^-( .*?|)$/u';

    public function parse(string $value)
    {
        if (preg_match('//u', $value) === false) {
            throw new ParseException('Not valid UTF-8');
        }

        $originalEncoding = mb_internal_encoding();
        mb_internal_encoding('UTF-8');

        try {
            $value = trim(str_replace(["\r\n", "\r"], "\n", $value));
            $lines = explode("\n", $value);

            return $this->parseByContext($lines, self::CONTEXT_NONE);
        } finally {
            mb_internal_encoding($originalEncoding);
        }
    }

    private function parseByContext(array $lines, int $context)
    {
        if ($context === self::CONTEXT_TEXT_VERBATIM) {
            return join("\n", $lines);
        }

        if ($context === self::CONTEXT_TEXT_FOLDED) {
            return join(' ', array_filter($lines));
        }

        return $this->doParse($lines);
    }

    private function doParse(array $lines)
    {
        $lineCount = count($lines);
        $currentLineNumber = -1;
        
        $data = [];

        while (true) {
            $currentLineNumber++;
            if ($currentLineNumber >= $lineCount) {
                return $data;
            }
            $currentLine = $lines[$currentLineNumber];

            if (preg_match(self::REGEX_EMPTY, $currentLine)) {
                continue;
            }

            try {
                if (preg_match(self::REGEX_DICT, $currentLine, $matches)) {
                    $key = trim($matches[1]);
                    $value = trim($matches[2]);

                    $data[$key] = $this->parseBlock($value, $lines, $currentLineNumber);
                    continue;
                }

                if (preg_match(self::REGEX_LIST, $currentLine, $matches)) {
                    $value = trim($matches[1]);

                    $data[] = $this->parseBlock($value, $lines, $currentLineNumber);

                    continue;
                }

                if (!is_array($data)) {
                    throw new ParseException('', $currentLineNumber, $currentLine);
                }

                if (count($data) > 0) {
                    throw new ParseException('Scalar in a list/dict context', $currentLineNumber, $currentLine);
                }

                $data = $this->parseScalar($lines, $currentLine, $currentLineNumber);
            } catch (ParseException $e) {
                throw $e->withLine($currentLineNumber);
            }

        }
    } // @codeCoverageIgnore

    private function parseScalar(array $lines, string $currentLine, int &$currentLineNumber)
    {
        if ($currentLine === self::REGEX_NULL) {
            return null;
        }

        if (preg_match(self::REGEX_INT_DEC, $currentLine, $matches)) {
            return (int) $matches[1];
        }

        if (preg_match(self::REGEX_INT_BIN, $currentLine, $matches)) {
            list(, $sign, $number) = $matches;
            return ($sign === '-' ? -1 : 1) * bindec($number);
        }

        if (preg_match(self::REGEX_INT_OCT, $currentLine, $matches)) {
            list(, $sign, $number) = $matches;
            return ($sign === '-' ? -1 : 1) * octdec($number);
        }

        if (preg_match(self::REGEX_INT_HEX, $currentLine, $matches)) {
            list(, $sign, $number) = $matches;
            return ($sign === '-' ? -1 : 1) * hexdec($number);
        }

        if (preg_match(self::REGEX_INF, $currentLine, $matches)) {
            return ($matches[0] ?? '' === '-' ? -1 : 1) * INF;
        }

        if (preg_match(self::REGEX_FLOAT, $currentLine, $matches)) {
            return (float) $matches[1];
        }

        if (preg_match(self::REGEX_BOOL, $currentLine, $matches)) {
            return $matches[1] === 'true';
        }

        if (preg_match(self::REGEX_DATETIME, $currentLine, $matches)) {
            $dtString = $matches[1];
            if (!preg_match('/' . self::REGEX_PART_DATE . '/u', $dtString)) {
                $dtString = '0001-01-01 ' . $dtString;
            }
            return new \DateTimeImmutable($dtString);
        }

        if (preg_match(self::REGEX_STRING_INLINE, $currentLine, $matches)) {
            return str_replace("''", "'", $matches[1]);
        }

        if (preg_match(self::REGEX_STRING_BLOCK, $currentLine, $matches)) {
            return $this->parseBlock($matches[1], $lines, $currentLineNumber);
        }

        if (preg_match(self::REGEX_LIST_INLINE, $currentLine, $matches)) {
            $result = [];
            $string = trim($matches[1]);
            while (mb_strlen($string) > 0) {
                if (!preg_match('/^' . self::REGEX_PART_INLINE_ELEMENT . '/u', $string, $matches)) {
                    throw new ParseException('', $currentLineNumber, $string);
                }

                $result[] = $this->doParse([$matches[1]]);

                $string = trim(mb_substr($string, mb_strlen($matches[0])));
            }

            return $result;
        }

        if (preg_match(self::REGEX_DICT_INLINE, $currentLine, $matches)) {
            $result = [];
            $string = trim($matches[1]);
            while (mb_strlen($string) > 0) {
                if (!preg_match('/^' . self::REGEX_PART_DICT_KEY . ' *' . self::REGEX_PART_INLINE_ELEMENT . '/u', $string, $matches)) {
                    throw new ParseException('', $currentLineNumber, $string);
                }

                $result[$matches[1]] = $this->doParse([$matches[2]]);

                $string = trim(mb_substr($string, mb_strlen($matches[0])));
            }

            return $result;
        }

        throw new ParseException('', $currentLineNumber, $currentLine);
    }

    private function parseBlock(string $value, array $lines, int &$currentLineNumber)
    {
        $contextMark = substr(trim($value), 0, 1);
        if ($contextMark === '#') {
            $contextMark = '';
        }

        $context = self::CONTEXTS[$contextMark] ?? self::CONTEXT_NONE;

        $lineCount = count($lines);

        if (in_array($contextMark, array_keys(self::CONTEXTS))) {
            $newLines = [];
            while (true) {
                $currentLineNumber++;
                if ($currentLineNumber >= $lineCount) {
                    $currentLineNumber--;
                    break;
                }
                $currentLine = $lines[$currentLineNumber];

                if (trim($currentLine) === '') {
                    $newLines[] = '';
                } else {
                    $blockIndent = $this->countIndent($currentLine);
                    if ($blockIndent > 0) {
                        $newLines[] = mb_substr($currentLine, self::INDENT);
                    } else {
                        $currentLineNumber--;
                        break;
                    }
                }
            }
        } else {
            $newLines = [$value];
        }

        return $this->parseByContext($newLines, $context);
    }

    private function countIndent(string $line): int
    {
        preg_match('/^( *)[^ ].*$/u', $line, $matches);

        $spaces = strlen($matches[1] ?? '');

        return (int) floor($spaces / self::INDENT);
    }
}
