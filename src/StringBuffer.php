<?php

namespace Avris\Suml;

final class StringBuffer
{
    /** @var string */
    private $buffer = '';

    public static function create(): self
    {
        return new self;
    }

    public function write(string $msg, ...$params): self
    {
        $this->buffer .= count($params) ? sprintf($msg, ...$params) : $msg;

        return $this;
    }

    public function writeln(string $msg = '', ...$params): self
    {
        $this->write($msg, ...$params);
        $this->buffer .= PHP_EOL;

        return $this;
    }

    public function get(): string
    {
        return $this->buffer;
    }

    public function length(): int
    {
        return mb_strlen($this->buffer);
    }

    public function __toString(): string
    {
        return $this->get();
    }

    public function clear(): self
    {
        $this->buffer = '';

        return $this;
    }
}
