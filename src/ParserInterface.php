<?php

namespace Avris\Suml;

interface ParserInterface
{
    public function parse(string $value);
}