<?php

namespace Avris\Suml;

interface DumperInterface
{
    public function dump($value): string;
}