<?php

namespace Avris\Suml;

use Avris\Suml\Exception\ParseException;
use Avris\Suml\Exception\RuntimeException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Suml\Suml
 */
class SumlTest extends TestCase
{
    /** @var ParserInterface|MockObject */
    private $parser;

    /** @var DumperInterface|MockObject */
    private $dumper;

    /** @var Suml */
    private $suml;

    protected function setUp()
    {
        $this->parser = $this->getMockBuilder(ParserInterface::class)->getMock();
        $this->dumper = $this->getMockBuilder(DumperInterface::class)->getMock();

        $this->suml = new Suml($this->parser, $this->dumper);
    }

    public function testInterfaces()
    {
        $this->assertInstanceOf(ParserInterface::class, $this->suml);
        $this->assertInstanceOf(DumperInterface::class, $this->suml);
    }

    public function testParse()
    {
        $this->parser->expects($this->once())->method('parse')->with('69')->willReturn(69);
        $this->assertSame(69, $this->suml->parse('69'));
    }

    public function testDump()
    {
        $this->dumper->expects($this->once())->method('dump')->with(69)->willReturn('69');
        $this->assertSame('69', $this->suml->dump(69));
    }

    public function testParseFile()
    {
        $this->parser->expects($this->once())->method('parse')->with('8' . PHP_EOL)->willReturn(8);
        $this->assertSame(8, $this->suml->parseFile(__DIR__ . '/data/scalar-input.suml'));
    }

    public function testParseFileNotFound()
    {
        $this->parser->expects($this->never())->method('parse');
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('File "/xyz" is not readable');
        $this->suml->parseFile('/xyz');
    }

    public function testParseFileException()
    {
        $filename = __DIR__ . '/data/scalar-input.suml';
        $this->parser->expects($this->once())->method('parse')->with('8' . PHP_EOL)
            ->willThrowException(new ParseException('msg', 3, 'foo'));
        $this->expectException(ParseException::class);
        $this->expectExceptionMessage(sprintf('Cannot parse file "%s" line 4 near `foo` : msg', $filename));
        $this->suml->parseFile($filename);
    }
}
