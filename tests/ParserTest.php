<?php

namespace Avris\Suml;

use Avris\Suml\Exception\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Suml\Parser
 */
class ParserTest extends TestCase
{
    const TESTCASES_DIR = __DIR__ . '/data/';
    const TESTCASES_WRONG_DIR = __DIR__ . '/data-wrong/';

    /** @var Parser */
    private $parser;

    protected function setUp()
    {
        $this->parser = new Parser();
    }

    /**
     * @dataProvider parseCasesProvider
     */
    public function testParse(string $input, string $output)
    {
        $this->assertEquals(
            $this->normaliseNewline(require self::TESTCASES_DIR . $output),
            $this->parser->parse(file_get_contents(self::TESTCASES_DIR . $input))
        );
    }

    public function parseCasesProvider()
    {
        return array_map(function ($filename) {
            return [
                basename($filename),
                basename(str_replace('-input.suml', '.php', $filename)),
            ];
        }, glob(self::TESTCASES_DIR . '*-input.suml'));
    }

    private function normaliseNewline($value)
    {
        $array = [$value];

        array_walk_recursive($array, function (&$item) {
            if (gettype($item) === 'string') {
                $item = str_replace(["\r\n", "\r"], "\n", $item);
            }
        });

        return $array[0];
    }

    /**
     * @dataProvider parseExceptionCasesProvider
     */
    public function testParseException(string $input, string $exceptionMessage)
    {
        $this->expectException(ParseException::class);
        $this->expectExceptionMessage($exceptionMessage);
        $this->parser->parse(file_get_contents(self::TESTCASES_WRONG_DIR . $input));
    }

    public function parseExceptionCasesProvider()
    {
        return array_map(function ($filename) {
            return [
                basename($filename),
                trim(mb_substr(file($filename)[0], 1)),
            ];
        }, glob(self::TESTCASES_WRONG_DIR . '*.suml'));
    }

    public function testParseNotUtf8Exception()
    {
        $this->expectException(ParseException::class);
        $this->expectExceptionMessage('Not valid UTF-8');
        $this->parser->parse(file_get_contents(__DIR__ . '/../logo.png'));
    }
}
