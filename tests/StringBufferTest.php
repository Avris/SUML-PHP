<?php

namespace Avris\Suml;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Suml\StringBuffer
 */
class StringBufferTest extends TestCase
{
    /** @var StringBuffer */
    private $buffer;

    public function setUp()
    {
        $this->buffer = new StringBuffer();
    }

    public function testConcatenation()
    {
        $this->assertEquals(
            'foobar',
            $this->buffer->write('foo')->write('bar')->get()
        );
    }

    public function testFormat()
    {
        $this->assertEquals(
            'foobar8',
            $this->buffer->write('foo')->write('bar%s', 5 + 3)->get()
        );
    }

    public function testNewLine()
    {
        $this->assertEquals(
            'foo' . PHP_EOL . 'bar 8 test A,B,C' . PHP_EOL,
            $this->buffer->writeln('foo')->writeln('bar %s test %s', 5 + 3, join(',', ['A', 'B', 'C']))->get()
        );
    }

    public function testFromStatic()
    {
        $this->assertEquals(
            'foo' . PHP_EOL,
            StringBuffer::create()->writeln('foo')->get()
        );
    }

    public function testToString()
    {
        $this->assertEquals(
            'foo' . PHP_EOL,
            (string) $this->buffer->writeln('foo')
        );
    }

    public function testClear()
    {
        $this->assertEquals(
            'bar',
            $this->buffer->write('foo')->clear()->write('bar')->get()
        );
    }
}
