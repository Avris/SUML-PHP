<?php

namespace Avris\Suml;

use Avris\Suml\Exception\DumpException;
use PHPUnit\Framework\TestCase;

class DumperTest extends TestCase
{
    const TESTCASES_DIR = __DIR__ . '/data/';
    const TESTCASES_WRONG_DIR = __DIR__ . '/data-wrong/';

    /** @var Dumper */
    private $dumper;

    protected function setUp()
    {
        $this->dumper = new Dumper();
    }

    /**
     * @dataProvider dumpCasesProvider
     */
    public function testDump(string $input, string $output)
    {
        $this->assertEquals(
            file_get_contents(self::TESTCASES_DIR . $output),
            $this->dumper->dump(require self::TESTCASES_DIR . $input)
        );
    }

    public function dumpCasesProvider()
    {
        return array_map(function ($filename) {
            return [
                basename($filename),
                basename(str_replace('.php', '-dump.suml', $filename)),
            ];
        }, glob(self::TESTCASES_DIR . '*.php'));
    }

    /**
     * @dataProvider dumpExceptionCasesProvider
     */
    public function testDumpException(string $input, string $exceptionMessage)
    {
        $this->expectException(DumpException::class);
        $this->expectExceptionMessage($exceptionMessage);
        $this->dumper->dump(require self::TESTCASES_WRONG_DIR . $input);
    }

    public function dumpExceptionCasesProvider()
    {
        return array_map(function ($filename) {
            return [
                basename($filename),
                trim(mb_substr(file($filename)[1], 2)),
            ];
        }, glob(self::TESTCASES_WRONG_DIR . '*.php'));
    }
}
