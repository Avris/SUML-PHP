<?php

return [
    'list' => [ 'foo', 8, true, 'lol,[]ek', new \DateTimeImmutable('2019-03-22 16:01:00+0100') ],
    'listComma' => ['fo#o' ],
    'listEmpty' => [],
    'dict' => [ 'foo' => 'FOO', 'number' => 8, 'bool' => true, 'escaping' => 'lol,[]ek', 'date' => new \DateTimeImmutable('2019-03-22 16:01:00+0100') ],
    'dictComma' => ['foo' => 'FOO'],
    'dictEmpty' => [],
];
