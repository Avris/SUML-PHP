<?php

return [
    'foo',
    [
        'bar' => 'BAR',
        'baz' => 'BAZ',
    ],
    8,
    'osiem#osiem',
    [
        'foo' => [
            'bar' => [
                'baz' => 'ok',
                'next' => [
                    'lorem' => 'ipsum',
                    'dodo' => 'rido',
                ],
            ],
            'hehe' => 7,
        ],
        'ok' => 2,
    ],
    'foo' . "\n" . 'bar',
    true,
];
