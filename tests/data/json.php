<?php

return [
    'scalar' => new class implements \JsonSerializable {
        public function jsonSerialize()
        {
            return 8;
        }
    },
    'dict' => new class implements \JsonSerializable {
        public function jsonSerialize()
        {
            return [
                'foo' => 'FOO',
                'bar' => 'BAR',
            ];
        }
    },
];
