<?php

namespace Avris\Suml\Exception;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Suml\Exception\DumpException
 */
class DumpExceptionTest extends TestCase
{
    /**
     * @dataProvider casesProvider
     */
    public function testMessage($value, string $expectedMessage)
    {
        $exception = new DumpException($value);
        $this->assertEquals($expectedMessage, $exception->getMessage());
        $this->assertSame($value, $exception->getValue());
    }

    public function casesProvider()
    {
        yield [STDIN, 'Cannot dump value: resource'];
        yield [new \stdClass(), 'Cannot dump value: object stdClass'];
        yield [new \Exception('foo'), 'Cannot dump value: object Exception "Exception: foo i..."'];
    }
}
