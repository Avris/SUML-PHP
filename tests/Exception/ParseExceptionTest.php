<?php

namespace Avris\Suml\Exception;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Suml\Exception\ParseException
 */
class ParseExceptionTest extends TestCase
{
    /**
     * @dataProvider casesProvider
     */
    public function testMessage(array $arguments, string $msg, string $msgWithFile, string $msgWithLine)
    {
        $exception = new ParseException(...$arguments);
        $this->assertEquals($msg, $exception->getMessage());
        $this->assertEquals($msgWithFile, $exception->withFilename('/lorem')->getMessage());
        $this->assertEquals($msgWithLine, $exception->withLine(68)->getMessage());
    }

    public function casesProvider()
    {
        yield [
            [],
            'Cannot parse',
            'Cannot parse file "/lorem"',
            'Cannot parse line 69',
        ];

        yield [
            ['foo'],
            'Cannot parse : foo',
            'Cannot parse file "/lorem" : foo',
            'Cannot parse line 69 : foo',
        ];

        yield [
            ['', 5],
            'Cannot parse line 6',
            'Cannot parse file "/lorem" line 6',
            'Cannot parse line 69',
        ];

        yield [
            ['', 5, 'foo'],
            'Cannot parse line 6 near `foo`',
            'Cannot parse file "/lorem" line 6 near `foo`',
            'Cannot parse line 69 near `foo`',
        ];
    }
}
