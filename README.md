# SUML for PHP

![SUML logo](logo.png)

This is a PHP implementation of a parser and a dumper of SUML: Simple & Unambiguous Markup Language.
You can check out its specification at [gitlab.com/Avris/SUML](https://gitlab.com/Avris/SUML)

## Installation

    composer require avris/suml

## Usage

    $suml = new \Avris\Suml\Suml;
    
    $parsed = $suml->parse("'input'");
    
    $parsedFile = $suml->parseFile(__DIR__ . '/filename.suml');
    
    $dump = $suml->dump(['foo' => 'bar', 'lorem' => true])

## Tests

    vendor/bin/phpunit
    
## Framework support

 - [Symfony](https://gitlab.com/Avris/SUML-symfony)
 
